// models/Appointment.js

const mongoose = require('mongoose');

const appointmentSchema = new mongoose.Schema({
    title: {
        type: String,
        required: true,
    },
    date: {
        type: Number, // Assuming it's a Unix timestamp or similar
        required: true,
    },
    time: {
        type: Number, // Assuming it's an integer representing time
        required: true,
    },
    email: {
        type: String,
        required: true,
    },
    number: {
        type: Number,
        required: true,
    },
    address: {
        type: String,
        required: true,
    },
    name: {
        type: String,
        required: true,
    },
});

const Appointmentmodel = mongoose.model('services', appointmentSchema);

module.exports = Appointmentmodel;

