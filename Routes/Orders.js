const express = require('express');
const Orders = express.Router();
const Ordermodel = require('../Model/Ordermodel'); // Update the path as needed

Orders.post('/', async (req, res) => {
    try {
        const orderData = {
            "productId": parseInt(req.body.productId),
            "email": req.body.email,
            "count": parseInt(req.body.count),
            "number": parseInt(req.body.number),
            "name": req.body.name,
        };

        // Create a new document using the Mongoose model
        const result = await Ordermodel.create(orderData);

        res.send("Data successfully inserted: " );
    } catch (error) {
        console.error(error);
        res.status(500).send("Internal Server Error");
    }
});

module.exports = Orders;
